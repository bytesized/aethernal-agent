require 'yaml'
require 'thor'
require 'docker-api'
require 'sqlite3'
require 'active_support'
require 'active_support/core_ext/hash/indifferent_access'
require 'active_support/core_ext/string'
require 'ostruct'
require 'etc'
require 'fileutils'
require 'socket'
require 'logger'
require 'bcrypt'
require 'net/http'
require 'terminal-table'

require 'aethernal_agent/app'
require 'aethernal_agent/loader'
require 'aethernal_agent/docker/volume'
require 'aethernal_agent/docker/port_binding'
require 'aethernal_agent/docker/container_settings'
require 'aethernal_agent/docker/env'
require 'aethernal_agent/template'
require 'aethernal_agent/operation'
require 'aethernal_agent/operation_pool'
require 'aethernal_agent/version'
require 'aethernal_agent/cli/core'

module AethernalAgent

  class << self
    attr_writer :logger

    def logger
      @logger ||= Logger.new($stdout)
    end

    def change_log_level(level = Logger::INFO)
      AethernalAgent.logger.level = level
    end
  end


  class Error < StandardError; end
end
