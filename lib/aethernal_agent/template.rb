require 'erb'
require 'aethernal_agent/filesystem'
require 'aethernal_agent/errors'

module AethernalAgent
  class Template
    include AethernalAgent::Filesystem
    include AethernalAgent::Errors

    attr_accessor :file, :parsed, :result, :template_path, :options

    def initialize(template_path, vars, options)
      self.template_path = template_path
      self.options = options

      vars.each do |k,v|
        AethernalAgent.logger.debug("Setting #{k} to #{v} #{v.class}")
        if [String, Symbol].include?(v.class)
          eval("@#{k}= '#{v}'")
        elsif v.is_a?(NilClass)
          eval("@#{k}= nil")
        else
          eval("@#{k}= #{v}")
        end
      end
    end

    def parse
      erb = ERB.new(File.open(self.template_path).read)
      self.result = erb.result(binding)

      if self.result
        self.parsed = true
        return true
      else
        return false
      end
    end

    def write_to(path)
      return false unless self.parsed

      begin
        file = File.open(path, "w")
        AethernalAgent.logger.debug("Writing template to #{path}")
        file << self.result
        file.close

        file_settings(path, self.options)

      rescue StandardError => e
        add_errors(e, path: path)
        return false
      end
    end
  end
end
