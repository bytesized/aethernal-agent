require 'aethernal_agent/utils'

module AethernalAgent
  module Apt
    def self.included klass
      klass.class_eval do
        include Utils
      end
    end

    def apt_update
      print_system("apt update")
    end

    def add_apt_ppa(name)
      print_system("add-apt-repository #{name} -y")
    end

    def apt_package(options = {})
      AethernalAgent.logger.info "Installing apt package(s) with options: #{options}"
      action = options[:action]
      action = :install if action.nil?

      packages = options[:packages]
      packages = packages.join(" ") if packages.is_a?(Array)

      unless wait_on_apt(180)
        self.add_errors("APT could not be started because lock was in use for more than 180 seconds")
      end

      case action
      when :install
        # Do this with popen so we can catch error output as well as normal output for add_errors
        print_system("DEBIAN_FRONTEND=noninteractive apt -o DPkg::Options::=--force-confdef install -qy #{packages}")
      when :remove
        print_system("DEBIAN_FRONTEND=noninteractive apt remove -qy #{packages}")
      when :autoremove
        print_system("DEBIAN_FRONTEND=noninteractive apt autoremove -qy")
      end
    end
  end
end
