module AethernalAgent
  module Webserver
    require 'sinatra/base'
    require 'sinatra/namespace'
    require 'aethernal_agent'

    class Api < Sinatra::Base
      include AethernalAgent::Utils
      register Sinatra::Namespace

      configure do
        set :loader, AethernalAgent::Loader.new
      end

      set :bind, '0.0.0.0'

      use Rack::Auth::Basic, "Protected Area" do |username, password|
        username == @@username && password == @@password
      end

      namespace "/v1" do
        get "/port/?:min?/?:max?" do
          min = params[:min] || 6000
          max = params[:max] || 40000
          return random_port(min..max).to_json
        end

        get "/version" do
          return AethernalAgent::VERSION
        end

        get "/plugins" do
          return settings.loader.loaded_manifests.collect{|x| x.to_h}.to_json
        end

        get "/operations" do
          {size: @@pool.operation_size}.to_json
        end

        get "/operations/:uuid" do
          op = @@pool.get_operation(params[:uuid])
          if op.present?
            {operation: op.attributes, error: nil}.to_json
          else
            status 404
            body do
              {operation: nil, error: "not found"}.to_json
            end
          end
        end

        post "/refresh_apache_configuration" do
          options = {custom_domains: params['custom_domains'], user: params['user']}
          apache = AethernalAgent::Apache.new(options)
          apache.ensure_base_config(options)
        end

        post "/plugins/:app_name/:method_name/?:wait?" do
          app = params['app_name']
          method = params['method_name']
          opts = JSON.parse(request.body.read)
          o = AethernalAgent::Operation.new(app, method, opts)

          unless params['wait'].nil?
            o.run
            o.to_json
          else
            @@pool.add_operation(o)
            o.uuid
          end
        end
      end

      def self.start(user, password, pool)
        @@username = user
        @@password = password
        @@pool = pool
        AethernalAgent.logger.debug("Starting API with username #{@@username} and password #{@@password}")
        run!
      end
    end
  end
end
