require 'aethernal_agent/utils'
require "aethernal_agent/webserver/api"

module AethernalAgent
  module Webserver
    class Core
      include AethernalAgent::Utils

      def start
        username = get_global_config("web_username")
        password = get_global_config("web_password")
        if username.nil? || password.nil?
          AethernalAgent.logger.info("No username or password found in config, creating one.")

          username = random_string
          password = random_string

          set_global_config("web_username", username)
          set_global_config("web_password", password)
        end

        AethernalAgent.logger.info("Starting AethernalAgent - v#{AethernalAgent::VERSION}")

        pool = AethernalAgent::OperationPool.new(3)
        threads = []
        threads << Thread.new { AethernalAgent::Webserver::Api.start(username, password, pool) }
        threads << Thread.new { pool.start }
        threads.each(&:join)
      end
    end
  end
end
