class AethernalAgent::Operation
  attr_accessor :uuid, :plugin_name, :method, :options, :result, :started_at, :completed_at, :exception, :backtrace, :errors

  def initialize(plugin_name, method, options)
    self.plugin_name = plugin_name
    self.method = method
    self.options = options
    self.uuid = SecureRandom.uuid
  end

  def run
    self.started_at = Time.now

    begin
      plugin = "AethernalAgent::#{self.plugin_name.camelcase}".constantize.new(self.options)
      self.result = plugin.send(self.method, self.options)
      if self.result.is_a?(Hash) && self.result.has_key?(:errors)
        self.errors = self.result[:errors]
      end
    rescue StandardError => e
      self.exception = e.message
      self.backtrace = e.backtrace
    end

    self.completed_at = Time.now
  end

  def success?
    !self.failed?
  end

  def failed?
    !self.exception.nil? || !self.errors.blank?
  end

  def is_completed?
    !self.completed_at.nil?
  end

  def is_started?
    !self.started_at.nil?
  end

  def attributes
    {method: self.method, plugin_name: self.plugin_name, options: self.options, uuid: self.uuid, result: self.result, success: self.success?, failed: self.failed?, is_completed: self.is_completed?, errors: self.errors, exception: self.exception, backtrace: self.backtrace, completed_at: self.completed_at, started_at: self.started_at}
  end

  def to_json(options ={})
    self.attributes.to_json
  end
end
