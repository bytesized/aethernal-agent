class AethernalAgent::Tautulli < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    directory(app_path, owner: self.user)
    directory(tautulli_config_path, owner: self.user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(home_folder_path("/apps/tautulli"), action: :delete)
      directory(home_folder_path("/.config/tautulli"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        port: opts['port'],
        tautulli_config_path: tautulli_config_path,
        password: opts['password'],
        user: self.user}
    end

    write_template(template_path('config.ini.erb'),
             tautulli_config_path('config.ini'),
             @vars,
             {owner: self.user})

    return create_return_args(@vars)
  end

  protected

  def tautulli_config_path(path ="/")
    File.join(home_folder_path(".config/tautulli"), path)
  end

end
