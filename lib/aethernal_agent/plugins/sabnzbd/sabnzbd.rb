class AethernalAgent::Sabnzbd < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    run_command("sudo pip3 install Cheetah3")
    run_command("sudo pip3 install portend")
    FileUtils.mv(app_path + "/SABnzbd-3.1.0/", home_folder_path("/apps/") + "/sabnzbd")
    directory(app_path, action: :delete)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(home_folder_path("/apps/sabnzbd"), action: :delete)
      directory(home_folder_path("/.config/sabnzbd"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        port: opts['port'],
        https_port: random_port(2000..8000),
        password: opts['password'],
        user: self.user}

      write_template(template_path('sabnzbd.ini.erb'),
                   sabnzbd_config_path('sabnzbd.ini'),
                   @vars,
                   {owner: self.user})
    end

    return create_return_args(@vars)
  end

  protected

  def sabnzbd_config_path(path ="/")
    File.join(home_folder_path(".config/sabnzbd"), path)
  end

end
