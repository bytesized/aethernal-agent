class AethernalAgent::Bazarr < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    unzip("bazarr*.zip")
    run_as('root',"mv #{app_path}/bazarr-master/* #{app_path}/")
    directory(app_path('bazarr-master'), action: :delete)
    file(app_path('bazarr-master.zip'), action: :delete)
    directory(app_path, owner: @user)
    directory(bazarr_config_path, owner: @user)
    directory(bazarr_config_path('data'), owner: @user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(app_path, action: :delete)
      directory(bazarr_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        password: opts['password'],
        hash: user_hash(opts),
        port: opts['port'],
        user: self.user}
    end

    run_command("cd #{app_path} && sudo pip3 install -r requirements.txt")
    stop
    write_template(template_path('config.ini.erb'),
                   bazarr_config_path('/data/config/config.ini'),
                   @vars,
                   {owner: self.user})

    start
    return create_return_args(@vars)
  end

  protected

  def user_hash(opts)
    return Digest::MD5.hexdigest(opts[:password])
  end

  def bazarr_config_path(path ="/")
    File.join(home_folder_path(".config/bazarr"), path)
  end

end
