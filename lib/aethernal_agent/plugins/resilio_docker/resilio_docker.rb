class AethernalAgent::ResilioDocker < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
  end

  def configure_app_user(options = {})
    install_packages(options)
    super(options) do |opts|
      write_template(template_path('resilio_docker.conf.erb'),
                   File.join(opts[:data_volume], ('sync.conf')),
                   opts.merge(home: '/mnt/sync'),
                   {owner: self.user})
    end
  end

  def uninstall_packages(options = {})
    super(options)
  end

  def remove_app_user(options = {})
    uninstall_packages(options)
    super(options)
  end

end
