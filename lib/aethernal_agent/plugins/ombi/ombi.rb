class AethernalAgent::Ombi < AethernalAgent::App

  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    directory(ombi_config_path, action: :create)
    super(options)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(app_path, action: :delete)
      directory(ombi_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)
    super(options) do |opts|
      ensure_permissions
      @opts = opts
      @opts[:config_path] = ombi_config_path
    end
    @timer = 0
    add_user_to_db
    @timer = 0
    get_ombi_token
    @timer = 0
    set_ombi_base_url
    AethernalAgent.logger.info("fininshed, returning args #{@opts}")
    self.restart
    return create_return_args(@opts)
  end

  protected

  def ombi_config_path(path ="/")
    File.join(home_folder_path(".config/ombi"), path)
  end

  def source_name
    self.manifest.package['direct_download']['target_name']
  end

  def ensure_permissions
    file(app_path("Ombi"), chmod: 0755, owner: self.user)
    file(app_path, owner: self.user)
    file(ombi_config_path, owner: self.user)
  end

  def add_user_to_db
    begin
      AethernalAgent.logger.info("Calling Ombi API to create our initial user for #{self.user}")
      uri = URI(ombi_api_url(@opts["port"], "Identity/Wizard"))
      req = Net::HTTP::Post.new(uri)
      req.content_type = 'application/json'
      req.body = {username: self.user, password: @opts[:password], usePlexAdminAccount: false}.to_json
      Net::HTTP.start(uri.host, uri.port) do |http|
        response = http.request(req)
      end
      AethernalAgent.logger.info('fininshed adding user')
    rescue => error
      AethernalAgent.logger.info(error.inspect)
      loop_till_success(:add_user_to_db)
    end
  end

  def get_ombi_token
    AethernalAgent.logger.info("Getting Ombi token for user #{self.user}")
    uri = URI(ombi_api_url(@opts["port"], "token"))
    req = Net::HTTP::Post.new(uri)
    req.content_type = 'application/json'
    req.body = {username: self.user, password: @opts[:password], rememberMe: false}.to_json
    Net::HTTP.start(uri.host, uri.port) do |http|
      response = http.request(req)
      @access_token = JSON.parse(response.body)['access_token']
      if !@access_token.present?
        loop_till_success(:get_ombi_token)
      end
    end
    AethernalAgent.logger.info('fininshed getting acces token')
  end

  def set_ombi_base_url
    AethernalAgent.logger.info("Updating Ombi settings for user #{@user}")
    uri = URI(ombi_api_url(@opts["port"], "Settings/ombi"))
    req = Net::HTTP::Post.new(uri)
    req['Authorization'] = "Bearer #{@access_token}"
    req.content_type = 'application/json'

    req.body = {baseUrl: "/ombi", wizard: false}.to_json
    Net::HTTP.start(uri.host, uri.port) do |http|
      response = http.request(req)
      if !response.body.in?(['true', true])
        AethernalAgent.logger.info("Response message: #{response.message}")
        loop_till_success(:set_ombi_base_url)
      end
    end
    AethernalAgent.logger.info('fininshed adding ombi base url')
  end

  def ombi_api_url(web_port, path)
    return "http://127.0.0.1:#{web_port}/api/v1/#{path}"
  end

  def loop_till_success(method)
    @timer += 1
    AethernalAgent.logger.info("#{method.to_s} not succesfull, looping #{@timer}th time.")
    sleep 60
    if @timer < 20
      method(method).call
    end
  end

end
