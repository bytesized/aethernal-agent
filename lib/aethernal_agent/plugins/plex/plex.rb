class AethernalAgent::Plex < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
  end

  def uninstall_packages(options = {})
    super(options)

    directory("/var/lib/plexmediaserver", action: :delete)
    directory("/usr/lib/plexmediaserver", action: :delete)
  end

  def remove_app_user(options = {})
    uninstall_packages(options)
    super(options)
  end

  def configure_app_user(options = {})
    #TODO: Do error handling here.
    install_packages(options)

    # Plex runs automatically after installing so let's stop it.
    run_command("systemctl daemon-reload")
    run_command("service plexmediaserver stop")

    directory("/var/lib/plexmediaserver", owner: self.user)
    directory("/usr/lib/plexmediaserver", owner: self.user)
    file("/lib/systemd/system/plexmediaserver.service", action: :delete)


    result = super(options) do |opts|
      container_domain = "#{get_global_config(:container_name)}.#{get_global_config(:hostname)}"
      write_template(template_path('Preferences.xml.erb'),
                     '/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Preferences.xml',
                     opts.merge(domain: container_domain),
                     {owner: self.user})
    end

    return result if result[:errors].present?

    # We will have to do this after the service files have been created so we can't do this inside the super itself
    reload_systemd_config
    start

    AethernalAgent.logger.debug("Sleeping to wait for Plex server start-up")
    sleep 30
    self.claim(result[:options])

    return create_return_args(result[:options])
  end

  def claim(options = {})
    AethernalAgent.logger.info("Claiming server")
    ensure_action_options('claim', options)
    run_command("curl -v -X POST http://127.0.0.1:32400/myplex/claim?token=#{options['claim_token']}")
    AethernalAgent.logger.info("Done claiming server")
    return create_return_args(options)
  end
end
