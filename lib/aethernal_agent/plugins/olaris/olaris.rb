class AethernalAgent::Olaris < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)

    unzip("olaris-linux-amd64-*.zip")
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory home_folder_path(".config/olaris"), action: :delete
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

     super(options) do |opts|
      directory home_folder_path("media/Movies"), action: :create
      directory home_folder_path("media/TV Shows"), action: :create

      run_as(self.user,"#{olaris_bin_path} user create --admin --username #{self.user} --password #{opts[:password]}")
      run_as(self.user,"#{olaris_bin_path} library create --name movies --path #{home_folder_path("media/Movies")} --media_type 0")
      run_as(self.user,"#{olaris_bin_path} library create --name tv --path '#{home_folder_path("media/TV Shows")}' --media_type 1")
    end
  end

  protected
  def olaris_bin_path
    app_path('bin/olaris')
  end
end
