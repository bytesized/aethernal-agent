class AethernalAgent::PlexDocker < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
  end

  def configure_app_user(options = {})
    install_packages(options)

    super do |opts|
      self.container_settings.env << "ADVERTISE_IP=http://#{self.container_domain}:#{opts[:host_port]}"

      write_template(template_path('Preferences.xml.erb'),
                     File.join(opts[:config_mount],'/Library/Application Support/Plex Media Server/Preferences.xml'),
                     opts.merge(domain: self.container_domain, container_name: get_global_config(:container_name)),
                     {owner: self.user})
    end
  end

  def uninstall_packages(options = {})
    super(options)
  end

  def remove_app_user(options = {})
    uninstall_packages(options)
    super(options)
  end

  def claim(options = {})
    AethernalAgent.logger.info("Claiming server")
    ensure_action_options('claim', options)
    run_command("curl -v -X POST http://127.0.0.1:32400/myplex/claim?token=#{options['claim_token']}")
    AethernalAgent.logger.info("Done claiming server")
    return create_return_args(options)
  end
end
