class AethernalAgent::Deluge < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
  end

  def uninstall_packages(options = {})
    super(options)
  end

  def remove_app_user(options = {})
    super do
      directory(home_folder_path(".config/deluge"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      directory( home_folder_path(".config/deluge"), owner: self.user)
      salt = SecureRandom.hex(20)

      vars = {
        identifier: SecureRandom.hex(20),
        daemon_port: opts['daemon_port'],
        port: opts['port'],
        password: opts['password'],
        user: opts['user'],
        home: home_folder_path,
        salt: salt,
        listen_port: opts['listen_port'],
        password_sha: sha_1_hash(opts['password'], salt)
      }

      write_template(template_path('core.conf.erb'),
                     deluge_config_path('core.conf'),
                     vars,
                     {owner: vars[:user]})

      write_template(template_path('web.conf.erb'),
                     deluge_config_path('web.conf'),
                     vars,
                     {owner: vars[:user]})

      write_template(template_path('auth.erb'),
                     deluge_config_path('auth'),
                     vars,
                     {owner: vars[:user]})

      write_template(template_path('hostlist.conf.erb'),
                     deluge_config_path('hostlist.conf'),
                     vars,
                     {owner: vars[:user]})

      write_template(template_path('ltconfig.conf.erb'),
                     deluge_config_path('ltconfig.conf'),
                     vars,
                     {owner: vars[:user]})
    end
  end

  protected
  def deluge_config_path(path ="/")
    File.join(home_folder_path(".config/deluge"), path)
  end
end
