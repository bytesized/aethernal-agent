class AethernalAgent::Vnc < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)

    apt_package(packages: "pulseaudio xscreensaver gvfs-common", action: :remove)
  end

  def uninstall_packages(options = {})
    super(options)
  end

  def remove_app_user(options = {})
    super do
      directory(home_folder_path(".vnc"), action: :delete)
    end
  end

  def configure_app_user(options = {})
   self.install_packages(options)

   super(options) do |opts|
      directory( home_folder_path(".vnc"), owner: self.user)

      # Make xstartup executable so vnc can use it
      aethernal_agent_file("xstartup", home_folder_path(".vnc/xstartup"), chmod: 0711, owner: self.user)

      run_command("echo #{opts[:password]} | vncpasswd -f > #{home_folder_path('.vnc/passwd')}")

      # VNC won't start if the passwd is too exposed.
      file_settings(home_folder_path('.vnc/passwd'), owner: self.user, chmod: 0600)

      opts.reverse_merge!(offset: opts[:port] - 5900, home: home_folder_path())

      set_ownership(home_folder_path('.config/'), self.user)
    end
  end
end
