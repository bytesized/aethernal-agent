module AethernalAgent
  module Docker
    class PortBinding

      attr_accessor :source, :target
      def initialize(source, target)
        self.source = source
        self.target = target
      end

      def self.create_from_options(manifest_options, opts)
        bindings = {}
        manifest_options.each do |k,v|
          if v.present?
            if v["docker"].present? && v["docker"]["source"].present?
              if v["docker"]["source"].to_s.include?("$port")
                v["docker"]["source"].gsub!("$port", opts[k].to_s)
              end
              bindings.merge!(PortBinding.new(v["docker"]["source"], opts[k]).to_docker)
            end
          end
        end

        return bindings
      end

      def to_docker
        { self.source => [{ "HostPort" => self.target.to_s}] }
      end
    end
  end
end
