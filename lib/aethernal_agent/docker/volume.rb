module AethernalAgent
  module Docker
    class Volume
      attr_accessor :source, :target
      def initialize(source, target)
        self.source = source
        self.target = target
      end

      def self.create_from_options(manifest_options, opts)
        volumes = []
        manifest_options.each do |k,v|
          if v.present?
            if v["docker"].present? && v["docker"]["volume"].present?
              volumes << Volume.new(opts[k], v["docker"]["volume"])
            end
          end
        end

        return volumes
      end

      def to_docker
        "#{self.source}:#{self.target}"
      end
    end
  end
end
