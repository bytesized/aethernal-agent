module AethernalAgent
  module Docker
    class ContainerSettings
      attr_accessor :env, :port_bindings, :volumes
      def self.build_from_options(manifest_options, opts)
        container = ContainerSettings.new
        container.env = Env.create_from_options(manifest_options, opts).collect(&:to_docker)
        container.port_bindings = PortBinding.create_from_options(manifest_options, opts)
        container.volumes = Volume.create_from_options(manifest_options, opts).collect(&:to_docker)
        return container
      end

      def create_container(name, image)
        AethernalAgent.logger.debug("Creating Docker container #{name} from image #{image} based on ContainerSettings")
        AethernalAgent.logger.debug("Env: #{self.env}, Ports; #{self.port_bindings}, Volumes: #{self.volumes}")

        ::Docker::Container.create(
          'name' => name,
          'Env' => self.env,
          'HostConfig' =>
          {
            'RestartPolicy' => {'Name' => 'unless-stopped'},
            'PortBindings' => self.port_bindings,
            'Binds' => self.volumes,
            'Volumes' => {"/data" => {},"/config" => {}},
          },
          'Image' => image,
          'Healthcheck' => {'Interval' => 120_000_000_000, 'StartPeriod' => 30_000_000_000}
        )
      end
    end
  end
end
