module AethernalAgent
  module Errors
    attr_accessor :global_errors

    def add_errors(error, vars = {})
      AethernalAgent.logger.debug("Adding errors: '#{error}'")
      self.global_errors ||=[]
      self.global_errors << {method: caller_locations(2,1)[0].label, error: error, args: vars}
    end

    def get_errors
      errors = self.global_errors
      self.global_errors = []
      return errors
    end
  end
end
