module AethernalAgent
  module Systemd
    class SystemdStatus < OpenStruct;end

    SYSTEMD_ACTIONS = ["stop", "start", "enable", "disable", "restart"]

    def run_user_systemctl(action, options = {})
      results = {}
      if options[:service_name]
        run_command("loginctl enable-linger #{self.user}")
        result = run_command("sudo -iu #{self.user} systemctl --user #{action} #{options[:service_name]}")
        if result == false
          add_errors("Could not '#{action}' service '#{options[:service_name]}'")
        end
        results[options[:service_name]] = result
      elsif self.manifest.services.present?
        self.manifest.services.each do |service|
          if File.exist?(service_file_path("#{service}.service"))
            result = run_command("sudo -iu #{self.user} systemctl --user #{action} #{service}")
            if result == false
              add_errors("Could not '#{action}' service '#{service}'")
            end
            results[service] = result
          else
            AethernalAgent.logger.debug("Service file service_file_path(#{service}.service did not exist, not trying to do action.")
          end
        end
      end

      return results
    end

    def create_service_file(options = {})
      AethernalAgent.logger.debug("Creating systemd file with options: #{options}")
      # Ensure dbus communication is possible as root
      run_command('echo \'export XDG_RUNTIME_DIR="/run/user/$UID"\' > /etc/profile.d/02-fix-xdg.sh')

      # Ensure that processes keep running once a user logs out
      run_command("loginctl enable-linger #{self.user}")

      directory( home_folder_path(".config/systemd/"), owner: self.user)
      directory( home_folder_path(".config/systemd/user/"), owner: self.user)

      must_start = options[:start] || true
      must_enable = options[:enable] || true

      if options[:template]
        write_template(
          template_path(options[:template]), #source
          service_file_path(options[:service_name]),
          options[:vars].reverse_merge(container_name: self.docker_container_name),
            {
              owner: options[:user]
            }
        )
      elsif options[:file]
        aethernal_agent_file(options[:file], service_file_path(options[:service_name]), owner: options[:user])
      end

      reload_systemd_config
      enable if must_enable
      restart if must_start
    end

    def reload_systemd_config
      run_command("systemctl --user daemon-reload")
    end

    def get_systemd_status(service_file)
      begin
        f = IO.popen("sudo -iu #{self.user} systemctl --user status #{service_file}")
        status = parse_systemd_status_text(f.readlines.join("\n"))
      rescue Errno::ENOENT => e
        return false
      ensure
        f.close
      end

      return status
    end

    def parse_systemd_status_text(text)
      s = SystemdStatus.new

      if text.present?
        loaded_status, service_file_and_enabled = text.match(/Loaded: (\w*) \((.*;)/).captures
        s.service_file_name, enabled_status = service_file_and_enabled.split(";")
        active_status,running_status = text.match(/Active: (\w*)\ \((.*)\)/).captures

        if text.include?("since")
          active_status,running_status,s.since = text.match(/Active: (\w*)\ \((.*)\) since (.*);/).captures
          s.main_pid = text.match(/Main PID: (\d*)/).captures.first
        end
      end

      s.loaded = loaded_status == "loaded"
      s.enabled = enabled_status == " enabled" # there is a space in the output here on purpose
      s.active = active_status == "active"
      s.running = running_status == "running"

      return s
    end

    def service_file_path(service_name)
      home_folder_path(File.join(".config/systemd/user/", service_name))
    end

      SYSTEMD_ACTIONS.each do |action|
        define_method(action) do |options = {}|
          if self.manifest.services.present?
            AethernalAgent.logger.info("#{action} on #{self.manifest.name} (#{self.manifest.services.join(' ')})")
            opts, errors = ensure_action_options(__method__,options)
            send("systemd_user_#{action}", opts)
            return create_return_args(opts)
          end
        end
      end

    # Dynamically create systemd_user_stop / start etc. actions
    def method_missing(method_name, *args)
      systemd_call = method_name.match(/^systemd_user_(\w*)$/)
      if systemd_call && systemd_call[1] && SYSTEMD_ACTIONS.include?(systemd_call[1])
        run_user_systemctl(systemd_call[1], args.first)
      else
        super
      end
    end
  end
end
