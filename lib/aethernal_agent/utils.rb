require 'json'
module AethernalAgent
  module Utils
    def check_manifest(manifest)
      if manifest.required_aethernal_agent_version
        AethernalAgent.logger.debug("Checking required aethernal_agent version")
        return Gem::Dependency.new('', manifest.required_aethernal_agent_version).match?('', AethernalAgent::VERSION)
      end
    end

    def run_command(command, options = {})
      AethernalAgent.logger.info "Running command: '#{command}"
      # TODO: Pipe this to get acutal output of commands to return error messages when stderr is used
      system(command)
    end

    def run_as(user, command)
      run_command("sudo -iu #{user} #{command}")
    end

    def print_system(command)
      AethernalAgent.logger.warn "pring_system is depcrated"
      AethernalAgent.logger.info "Running command: '#{command}'"
      system(command)
    end

    def random_string(length = 16)
      o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten
      string  =  (0...length).map{ o[rand(o.length)] }.join
      return string
    end

    def apt_running?
      system("lsof /var/lib/dpkg/lock > /dev/null")
    end

    def wait_on_apt(tries = 120)
      AethernalAgent.logger.info("Waiting on APT to become ready")

      while apt_running? do
        AethernalAgent.logger.info("APT is still running, sleeping.") if tries % 10 == 0
        tries -= 1
        if tries <= 0
          AethernalAgent.logger.info("Ran out of tries, exiting.")
          return false
        end
        sleep 1
      end

      return true
    end

    def wait_on_file(path, tries = 20)
      AethernalAgent.logger.info("Waiting on file #{path} to exist")

      while !File.exist?(path) do
        AethernalAgent.logger.info("File does not exist yet, sleeping.") if tries % 10 == 0
        tries -= 1
        if tries <= 0
          AethernalAgent.logger.info("Ran out of tries, exiting.")
          return false
        end
        sleep 1
      end

      return true
    end

    def wait_on_port(port, tries = 120)
      AethernalAgent.logger.info("Waiting on port #{port} to be in use.")

      while port_is_free(port) do
        AethernalAgent.logger.info("Port not in use yet, sleeping.") if tries % 10 == 0
        tries -= 1
        if tries <= 0
          AethernalAgent.logger.info("Ran out of tries, exiting.")
          return false
        end
        sleep 1
      end

      # Wait a bit extra to ensure processes are fully loaded
      sleep 2
      return true
    end


    def port_is_free(port)
      return false unless port.present?
      begin
        server = TCPServer.new("127.0.0.1", port.to_i)
        server.close
      rescue Errno::EADDRINUSE, Errno::EACCES, TypeError, Errno::ECONNREFUSED, Errno::EHOSTUNREACH
        return false
      end
      return true
    end

    def random_port(range = (20000..50000))
      port = range.to_a.sample
      if port && port_is_free(port)
        return port.to_i
      else
        random_port(range)
      end
    end

    def get_global_config(key)
      AethernalAgent.logger.debug("Getting value for key '#{key}'")

      begin
        config = JSON.parse(File.read(global_config_path))
      rescue Errno::ENOENT
        config = {}
      end

      AethernalAgent.logger.debug("Value is '#{config[key.to_s]}'")
      return config[key.to_s]
    end

    def set_global_config(key, value)
      AethernalAgent.logger.debug("Setting key '#{key}' to value '#{value}'")

      if File.exists?(global_config_path)
        begin
          config = JSON.parse(File.read(global_config_path))
        rescue StandardError => e
          raise "Could not read/parse global config: '#{e}'"
        end
      else
        AethernalAgent.logger.debug("Config file does not exist yet, creating new.")
        config = {}
      end

      config[key.to_s] = value
      begin
        file = File.open(global_config_path, "w")
        file.write(JSON.dump(config))
        file.close
      rescue StandardError => e
        raise "Could not write out global config file: '#{e}'"
      end

      FileUtils.chown("root","root", global_config_path)
    end

    def ubuntu_release
      AethernalAgent.logger.debug("Looking for Linux version number.")
      version_number = %x( lsb_release -sr )
      AethernalAgent.logger.debug("Returning version number: #{version_number.to_s.gsub('.', '')}")
      version_number.to_s.gsub('.', '').strip
    end

    def prepare_test_config
      File.open(global_config_path, "w") do |f|
        f.write({hostname: "test-server", container_name: "test-container"}.to_json)
      end
    end

    def global_config_path
      "/etc/aethernal_agent.config"
    end

    def docker_container_name
      "#{self.user}.#{self.manifest.plain_name}"
    end

    def container_domain
      "#{get_global_config(:container_name)}.#{get_global_config(:hostname)}"
    end

    def current_xdg_runtime_dir
      "export XDG_RUNTIME_DIR=/run/user/#{get_current_uid}"
    end

    def get_current_uid
      `id`.split("(")[0].split("=")[1]
    end

    def systemd_text_for_service(service_name)
      run_systemd_plain("status", service_name)
    end

    def run_systemd_plain(action, service_name)
      `#{current_xdg_runtime_dir} && systemctl --user #{action} #{service_name}`
    end
  end
end
