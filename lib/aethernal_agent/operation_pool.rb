class AethernalAgent::OperationPool
  attr_accessor :operations

  def initialize(size)
    @size = size
    @jobs = Queue.new
    self.operations = {}
  end

  def start
    @pool = Array.new(@size) do |i|
      Thread.new do
        Thread.current[:id] = i
        catch(:exit) do
          loop do
            op = @jobs.pop
            AethernalAgent.logger.debug("Worker #{i} - Picking up job #{op.uuid}")
            op.run
          end
        end
      end
    end
  end

  def queue_size
    @jobs.size
  end

  def operation_size
    self.operations.keys.size
  end

  def add_operation(op)
    @jobs << op
    self.operations[op.uuid] = op
  end

  def get_operation(uuid)
    op = self.operations[uuid]
    if !op.nil? && op.is_completed?
      self.operations.delete(uuid)
    end

    return op
  end
end
