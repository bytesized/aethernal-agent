require "minitest/autorun"
require 'aethernal_agent'

class AethernalAgent::Test
  def initialize(options = {})
  end

  def talker(options = {})
    return options[:talk]
  end

  def error(options ={})
    return {errors: {talk: ["error"]}}
  end
end

describe AethernalAgent::Operation do
  it "should generate a uuid" do
    b =AethernalAgent::Operation.new("test","test", {})
    _(b.uuid).wont_equal nil
  end

  it "should store errors" do
    op = AethernalAgent::Operation.new("test","error", {})
    op.run
    _(op.errors).must_include(:talk)
  end


  it "should be able to list all attributes" do
    b =AethernalAgent::Operation.new("test","test", {})
    _(b.attributes.keys).must_include(:completed_at)
    _(b.attributes.keys).must_include(:is_completed)
    _(b.attributes.keys).must_include(:errors)
    _(b.attributes.keys).must_include(:exception)
    _(b.attributes.keys).must_include(:result)
    _(b.attributes.keys).must_include(:backtrace)
    _(b.attributes.keys).must_include(:started_at)
    _(b.is_completed?).must_equal false
  end

  it "should be able to convert to json" do
    b =AethernalAgent::Operation.new("test","test", {}).to_json
    _(b).must_include "backtrace"
    _(b).must_include "is_completed"
    _(b).must_include "uuid"
  end

  describe "Running in a pool" do
    before(:each) do
      @pool = AethernalAgent::OperationPool.new(1)
    end

    it "should accept new jobs" do
      op = AethernalAgent::Operation.new("test","talker", {talk: "testing"})
      _(op.started_at).must_be_nil
      _(op.is_started?).must_equal false
      @pool.add_operation(op)
      _(@pool.queue_size).must_equal 1
      _(@pool.operation_size).must_equal 1
      _(@pool.get_operation(op.uuid)).must_equal op
      @pool.start
      sleep 0.2
      _(op.is_started?).must_equal true
      _(@pool.queue_size).must_equal 0
      _(@pool.operation_size).must_equal 1
      _(@pool.get_operation(op.uuid).result).must_equal "testing"
      _(@pool.operation_size).must_equal 0
      _(op.started_at).wont_equal nil
    end

    it "should rescue exceptions" do
      op = AethernalAgent::Operation.new("test","notexist", {talk: "testing"})
      @pool.add_operation(op)
      _(@pool.operation_size).must_equal 1
      @pool.start
      sleep 0.1
      op = @pool.get_operation(op.uuid)
      _(op.failed?).must_equal true
      _(op.exception).wont_equal nil
      _(op.backtrace).wont_equal nil
    end
  end
end
