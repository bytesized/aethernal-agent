require "minitest/autorun"
require 'aethernal_agent'
require File.expand_path(File.join(File.dirname(__FILE__), 'fixtures/dockerapp/docker_app'))

describe "Docker functionality" do
  it "should create volumes" do
    volumes = AethernalAgent::Docker::Volume.create_from_options(HashWithIndifferentAccess.new({mount: {docker: {volume: "/docker/path"}}}), HashWithIndifferentAccess.new({mount: "/host/path"}))
    _(volumes.first.to_docker).must_equal "/host/path:/docker/path"
  end

  it "should create ports" do
    bindings = AethernalAgent::Docker::PortBinding.create_from_options(HashWithIndifferentAccess.new({port: {docker: {source: "32111/tcp"}}}), HashWithIndifferentAccess.new({port: "5555"}))
    _(bindings).must_equal({"32111/tcp" => [{"HostPort" => "5555"}]})
  end

  it "should create envs" do
    envs = AethernalAgent::Docker::Env.create_from_options(HashWithIndifferentAccess.new({port: {docker: {env: "PORT_NUMBER"}}}), HashWithIndifferentAccess.new({port: "4411"}))
    _(envs.collect(&:to_docker)).must_equal(["PORT_NUMBER=4411"])
  end

  it "should create Container settings" do
    @app = DockerApp.new
    cs = AethernalAgent::Docker::ContainerSettings.build_from_options(@app.manifest["actions"]["configure_app_user"], {'transcode_mount' => "/tmp/transcode", 'claim_token' => "CLAIMMEPLOX", 'port' => 9999, 'fake_port' => 8888, 'user' => 'root', 'dynamic_port' => 9999})
    _(cs.port_bindings).must_equal({"9999/tcp"=>[{"HostPort"=>"9999"}], 32400=>[{"HostPort"=>"9999"}], 32300 => [{"HostPort" => "8888"}]})
    _(cs.env).must_equal(["PUID=0", "PLEX_CLAIM=CLAIMMEPLOX"])
    _(cs.volumes).must_include("/tmp/transcode:/transcode")
  end
end
