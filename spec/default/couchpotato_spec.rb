require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Couchpotato user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test couchpotato remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test couchpotato configure_app_user testuser \'{"port": 5345, "password": "hallo"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(5345) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/couchpotato") do
    it { should be_directory }

  end

  describe file("/home/testuser/.config/couchpotato/config.ini") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should contain 'username = testuser' }
  end

  describe file("/home/testuser/.config/systemd/user/couchpotato.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/couchpotato") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/couchpotato/CouchPotato.py") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/couchpotato.conf") do
    it { should be_file }
  end
end
