require 'spec_helper'
require 'aethernal_agent'

describe "Configuring an Ombi user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test ombi remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test ombi configure_app_user testuser \'{"port": 6588}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("Ombi") do
    its(:user) { should eq "testuser" }
  end

  describe port(6588) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/ombi") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/ombi.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/ombi") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  # describe file("/home/testuser/apps/radarr/Radarr.exe") do
  #   it { should be_file }
  #   it { should be_owned_by "testuser" }
  #   it { should be_executable }
  # end

  describe file("/etc/apache2/users/testuser/ombi.conf") do
    it { should be_file }
  end
end
