require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Tautulli user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test tautulli remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test tautulli configure_app_user testuser \'{"port": 5345, "password": "hallo"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(5345) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/tautulli") do
    it { should be_directory }

  end

  describe file("/home/testuser/.config/tautulli/config.ini") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should contain 'http_port = 5345' }
  end

  describe file("/home/testuser/.config/systemd/user/tautulli.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/tautulli") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/tautulli/Tautulli.py") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/tautulli.conf") do
    it { should be_file }
  end
end
