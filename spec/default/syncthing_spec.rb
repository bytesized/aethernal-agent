require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a syncthing user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test syncthing remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test syncthing configure_app_user testuser \'{"port": 29960}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("syncthing") do
    its(:user) { should eq "testuser" }
  end

  describe port(29960) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/syncthing") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/syncthing/config.xml") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /:29960/ }
    its(:content) { should match /testuser/ }
  end

  describe package('syncthing') do
    it { should be_installed }
  end
end

describe "Removing a Syncthing installation" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test syncthing remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe file("/home/testuser/.config/syncthing/config.xml") do
    it { should_not exist}
  end
end
