require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a VNC user" do

  bundle_update
  describe command('sudo /vagrant/bin/run-aa-for-test vnc remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test vnc install_packages testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test vnc configure_app_user testuser \'{"port": 5905}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("Xtightvnc") do
    its(:user) { should eq "testuser" }
  end

  describe process("xfwm4") do
    its(:user) { should eq "testuser" }
  end

  describe port(5905) do
    it { should be_listening }
  end

  describe file("/home/testuser/.vnc") do
    it { should be_directory }
  end

  describe file("/home/testuser/.vnc/passwd") do
    it { should be_file }
    it { should be_mode 600 }
  end

  describe file("/home/testuser/.vnc/xstartup") do
    it { should be_file }
    it { should be_executable }
  end

  describe file("/home/testuser/.config/systemd/user/vnc.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe package('tightvncserver') do
    it { should be_installed }
  end

  describe package('xfce4') do
    it { should be_installed }
  end

  describe package('lxterminal') do
    it { should be_installed }
  end
end
