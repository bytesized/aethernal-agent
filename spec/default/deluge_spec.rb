require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a deluge user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test deluge remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test deluge install_packages') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test deluge configure_app_user testuser \'{"port": 9960, "daemon_port": 9959}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("deluged") do
    its(:user) { should eq "testuser" }
  end

  describe port(9959) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/deluge") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/deluge/core.conf") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/deluge/web.conf") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/deluge/auth") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/deluge/hostlist.conf") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/deluge/ltconfig.conf") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/systemd/user/deluged.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/systemd/user/deluge-web.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end
end
