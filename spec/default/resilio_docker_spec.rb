require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Resilio Docker installation" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test resilio_docker remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test resilio_docker configure_app_user testuser \'{"port": 4160, "data_volume": "/home/testuser/sync-data"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe docker_image('resilio/sync') do
    it { should exist }
  end

  describe docker_container('testuser.resilio_docker') do
    it { should exist }
    it { should be_running }
  end

  describe file("/home/testuser/.config/systemd/user/resilio_docker.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /testuser.resilio_docker/ }
  end

end
