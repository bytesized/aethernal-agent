require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Radarr user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test radarr remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test radarr configure_app_user testuser \'{"port": 5454}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("Radarr") do
    its(:user) { should eq "testuser" }
  end

  describe package('mediainfo') do
    it { should be_installed }
  end

  describe port(5454) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/Radarr") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/radarr.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/radarr") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/radarr/Radarr") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should be_executable }
  end

  describe file("/etc/apache2/users/testuser/radarr.conf") do
    it { should be_file }
  end
end
