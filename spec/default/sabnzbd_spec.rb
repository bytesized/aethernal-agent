require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Sabnzbd user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test sabnzbd remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test sabnzbd configure_app_user testuser \'{"port": 2895, "password": "hallo"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(2895) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/sabnzbd") do
    it { should be_directory }

  end

  describe file("/home/testuser/.config/sabnzbd/sabnzbd.ini") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should contain 'port = 2895' }
  end

  describe file("/home/testuser/.config/systemd/user/sabnzbd.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/sabnzbd") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/sabnzbd/SABnzbd.py") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/sabnzbd.conf") do
    it { should be_file }
  end
end
